import "./Expenses.css";
import ExpenseItem from "./ExpenseItem";
import Card from "./UI/Card";
import ExpensesFilter from "./ExpensesFilter";
import { useState } from "react";
//import expenses from "./../App";

//we use props to pass attributes or properties components
function Expenses(props) {
  const [filteredYear, setFilteredYear] = useState("2020");

  const filterChangeHandler = (selectedYear) => {
    setFilteredYear(selectedYear);
  };

  return (
    <div>
      <Card className="expenses">
        {/*you can put hard code too in the key attribute, props=take this props. name of the props=expenses"OLD", 
      NEW=items. then, extract props from App father*/}
        <ExpensesFilter
          selected={filteredYear}
          onChangeFilter={filterChangeHandler}
        />
        {props.items.map((expense) => (
          <ExpenseItem
            title={expense.title}
            amount={expense.amount}
            date={expense.date}
            //map is a method that modifies the array.
          />
        ))}
        {/*hardcode mode in an array app.js
        <ExpenseItem
          title={props.items[0].title}
          amount={props.items[0].amount}
          date={props.items[0].date}
        ></ExpenseItem>*/}
      </Card>
    </div>
  );
}
export default Expenses;
