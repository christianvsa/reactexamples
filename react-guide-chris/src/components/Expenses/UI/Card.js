import "./Card.css";

function Card(props) {
  //this add all the props className to the card
  const classes = "card " + props.className;
  //props.children helps to wrap our components and not duplicates code
  return <div className={classes}>{props.children}</div>;
}
export default Card;
