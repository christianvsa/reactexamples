import React, { useState } from "react"; //importing Objects and part of that objects
import "./ExpenseItem.css";
import ExpenseDate from "./ExpenseDate";
import Card from "./UI/Card";

/*a parameter is "props" to use the keys in App to use the attributes, 
we use props to allaows to pass data from another component and make it reusable,
props only can pass from parent to child: App to*/

function ExpenseItem(props) {
  const [title, setTitle] = useState(props.title);
  /*const expenseDate = new Date(2021, 2, 28);
  const expenseTitle = "Car Insurance";
  const expenseAmount = 294.67; */
  //when you want a change or update in your data use "useState"
  // title= the name of our current value that we are managed in useState
  //not call useSate in nested (anidar,nido) functions, here we calling the value of useState
  const clickHandler = () => {
    //new value we want
    setTitle("Updated!!");
    console.log(title);
  };

  return (
    <Card className="expense-item">
      <ExpenseDate date={props.date} />
      <div className="expense-item__description">
        <h2>{props.title}</h2>
        {/*takes the father props--- here was (props.title) JSX new Value of useState*/}
        <div className="expense-item__price">${props.amount}</div>
        {/*onClick are eventslistener*/}
        <button onClick={clickHandler}>clickexample</button>
      </div>
    </Card>
  );
}
export default ExpenseItem;
