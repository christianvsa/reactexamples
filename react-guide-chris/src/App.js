import { useState } from "react";
import Expenses from "./components/Expenses/Expenses";
import NewExpense from "./components/NewExpense/NewExpense";

//everytime you made a function type the export default right away
//this is optional is the same "const App= () => {.....}"
// const expenses OLDER
const DUMMY_EXPENSES = [
  {
    id: "e1",
    title: "Toilet Paper",
    amount: 94.12,
    date: new Date(2020, 7, 14),
  },
  { id: "e2", title: "New TV", amount: 799.49, date: new Date(2021, 2, 12) },
  {
    id: "e3",
    title: "Car Insurance",
    amount: 294.67,
    date: new Date(2021, 2, 28),
  },
  {
    id: "e4",
    title: "New Desk (Wooden)",
    amount: 450,
    date: new Date(2021, 5, 12),
  }
];
const App = () => {
  const [expenses, setExpenses] = useState(DUMMY_EXPENSES);

  const addExpenseHandler = (expense) => {
    setExpenses((prevExpenses) => {
      return [expense, ...prevExpenses];
    });
  };
  return (
    //React.createElement('div',{}, React.createElement...)
    <div>
      <NewExpense onAddExpense={addExpenseHandler} />
      {/*in the key "title, items=" you can put any name that contain attributes, here was Expenses.
      then you have to import and bring that props name from expenses ARRAY =items then tell to father what items means in this file*/}
      <Expenses items={expenses} />
    </div>
  );
}

export default App;
