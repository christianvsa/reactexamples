import ReactDOMClient from 'react-dom/client';
//Old React Version
//import ReactDom from 'react-dom;
import './index.css';
import App from './App';

//Old React Version
//ReactDOM.render(<App />, document.getElementById('root'));

//New React Version
const container = document.getElementById ('root');

const root = ReactDOMClient.createRoot(container);
root.render(<App />);
 


